import Head from "next/head"
import Menu from "../components/menu";

function Home() {
    return (
    <div>
        <Head>
           <meta charSet="uft-8" />
           <meta name="robots" content="index,follow"/>
           <meta name="description" content="projeto conduit"/>
           <meta name="author" content="Pedro Nunes"/>
           <meta name="viewport" content="initial-scale=1.0,width=device width"/>
           
           <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.0/css/all.min.css" />
           <title>Home-Conduit</title>
        </Head>
       <Menu />
       <section className="top">
           <div className="max-width">
               <div className="top-content">
                   <div className="text-1">
                        conduit
                   </div>
                   <div className="text-2">
                       A place to share your knowledge.
                   </div>
               </div>
           </div>

       </section>

       <section classname="col-md-9">
         <div class="col-md-8">
                <div class="feed-toggle">
                    <ul class="nav nav-pills outline-active">
                        <li class="nav-item">
                            <a class="nav-link disabled" href="">Your Feed</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="">Global Feed</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="article-preview">
                    <div class="article-meta">
                        
                        <div class="info">
                            <a href="" class="author">Eric Simons</a>
                            <span class="date">January 20th</span>
                        </div>
                        
                    </div>
                    <a href="" class="preview-link">
                        <h1>How to build webapps that scale</h1>
                        <p>This is the description for the post.</p>
                        <span>Read more...</span>
                    </a>
                </div>

                <div class="article-preview">
                    <div class="article-meta">
                       
                        <div class="info">
                            <a href="" class="author">Albert Pai</a>
                            <span class="date">January 20th</span>
                        </div>
                        
                    </div>
                    <a href="" class="preview-link">
                        <h1>The song you won't ever stop singing. No matter how hard you try.</h1>
                        <p>This is the description for the post.</p>
                        <span>Read more...</span>
                    </a>
                </div>
                <div class="col-md-3">
                <div class="sidebar">
                    <p>Popular Tags</p>

                    <div class="tag-list">
                        <a href="" class="tag-pill tag-default">programming</a>
                        <a href="" class="tag-pill tag-default">javascript</a>
                        <a href="" class="tag-pill tag-default">emberjs</a>
                        <a href="" class="tag-pill tag-default">angularjs</a>
                        <a href="" class="tag-pill tag-default">react</a>
                        <a href="" class="tag-pill tag-default">mean</a>
                        <a href="" class="tag-pill tag-default">node</a>
                        <a href="" class="tag-pill tag-default">rails</a>
                    </div>
                </div>
            </div>

            

       </section>


       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
       <script src="custom.js"> </script>
    </div>
    )
  }
  
  export default Home