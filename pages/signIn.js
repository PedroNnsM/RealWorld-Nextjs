import { useState } from 'react';
import Head from "next/head"
import Menu from "../components/menu";

function Login(){

   const [dataForm, setDataForm] = useState({
   
      email: '',
      password: ''
   });

      const onChangeInput = e => setDataForm( { ...dataForm,[e.target.name]:e.target.value });

      const sendContact= async e =>{
         e.preventDefault();
        // console.log(dataForm.name);
       //  console.log(dataForm.email);
        // console.log(dataForm.password);
      }
   
      return (
    <div>
        <Head>
           <meta charSet="uft-8" />
           <meta name="robots" content="index,follow"/>
           <meta name="description" content="projeto conduit"/>
           <meta name="author" content="Pedro Nunes"/>
           <meta name="viewport" content="initial-scale=1.0,width=device width"/>
           <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.0/css/all.min.css" />
           <title>sing Up-Conduit</title>
        </Head>
       <Menu />
      
         <section className='conteiner'>
            <div className='row'>
               <h2 className='title'>Sign In</h2>
               <div className='conteiner-content'>                  
                     <p><a href="/signUp">Need an account?</a></p>                  
               </div>
               <form onSubmit={sendContact}>
                  <div className='fields'>
                     <div className='field name'>
                         <input type="email" name="email" placeholder="Email"onChange={onChangeInput} value={dataForm.email} />
                      </div>
                  </div>
                  <div className='fields'>
                     <div className='field name'>
                           <input type="password" name="password" placeholder="password"onChange={onChangeInput} value={dataForm.password} />
                     </div>
                  </div>                  
                     <button type="submit">Sign In</button>                            
               </form>
            </div>
         </section>

       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
       <script src="custom.js"> </script>
    </div>
    )
};   
  
  export default Login