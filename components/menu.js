import Link from "next/link";

const linkStyle = {
    marginLeft:50
}

const Menu = () => (

    <div>
        <nav className="navbar">
            <div className="max-width">
                <div className="logo">
                    <a href="/">conduit</a>
                </div>
                <ul className="menu">
                    <li><a href="/" className="menu-btn">Home </a></li>
                    <li><a href="/signIn" className="menu-btn">Sign In </a></li>
                    <li><a href="/signUp" className="menu-btn">Sign Up </a></li>
                </ul>
                        <div className="menu-btn">
                        <i class="fa-solid fa-bars"></i>
                </div>
            </div>
        </nav>
    </div>
);

export default Menu;